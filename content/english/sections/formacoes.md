---
enable: true
title: "Formações"
description: "O Fermento existe para acompanhar-vos a melhorarem a vossa organização, estratégia, ação, comunicação e Segurança"

# Testimonials
testimonials:
  - name: "Marvin McKinney"
    designation: "Web Designer"
    avatar: "/images/avatar-sm.png"
    content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui iusto illo molestias, assumenda expedita commodi inventore non itaque molestiae voluptatum dolore, facilis sapiente, repellat veniam."

  - name: "Marvin McKinney"
    designation: "Web Designer"
    avatar: "/images/avatar-sm.png"
    content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui iusto illo molestias, assumenda expedita commodi inventore non itaque molestiae voluptatum dolore, facilis sapiente, repellat veniam."

  - name: "Marvin McKinney"
    designation: "Web Designer"
    avatar: "/images/avatar-sm.png"
    content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui iusto illo molestias, assumenda expedita commodi inventore non itaque molestiae voluptatum dolore, facilis sapiente, repellat veniam."

  - name: "Marvin McKinney"
    designation: "Web Designer"
    avatar: "/images/avatar-sm.png"
    content: "Lorem ipsum dolor sit amet consectetur adipisicing elit. Qui iusto illo molestias, assumenda expedita commodi inventore non itaque molestiae voluptatum dolore, facilis sapiente, repellat veniam."

# don't create a separate page
_build:
  render: "never"
---
