---
enable: true
title: "FORMAÇÕES"
description: "O Fermento existe para acompanhar-vos a melhorarem a vossa organização, estratégia, ação, comunicação e Segurança"

# Testimonials
formacoes:
  - name: "Ação"
    avatar: "/images/acao.png"
    content: "Ninguém nasce como especialista em ação direta e o sistema socioeconómico em que vivemos educa-nos a obedecer em vez de agir. Grupos que chegam à conclusão que devem tomar ação direta podem sentir-se incapazes por falta de experiência e por causa dos possíveis riscos políticos."

  - name: "Estratégia"
    avatar: "/images/estrategia.png"
    content: "Lidando com a realidade volátil, incerta, complexa e ambígua, os nossos movimentos estão a ser constantamente desafiadas para criar estratégias e ter um processo coletivo de aprendizagem."

  - name: "Organização"
    avatar: "/images/organizacao.png"
    content: "Todos os grupos têm dificuldades em recrutar, consolidar e manter os seus membros enquanto sustentam a sua eficácia. Sob pressão externa, encontramo-nos numa luta constante para constuir capacidade interna para alimentar os nossos movimentos."

  - name: "Comunicação"
    avatar: "/images/comunicacao.png"
    content: "Para mudarmos o mundo, precisamos de mudar as histórias que se contam na sociedade. Os media nem sempre estão connosco e, por isso, temos de investir numa estratégia forte e numa equipa sólida de comunicação. Não somos profissionais de comunicação mas temos bastante experiência do ativismo de base."

  - name: "Segurança"
    avatar: "/images/seguranca.png"
    content: Todos os grupos que tomam ação são confrontados com a vigilância e repressão do sistema. Muitas vezes sentimo-nos impotentes, paralisadas ou em pânico, quando cedemos a esses sentimentos perdemos. Precisamos de nos adaptar à realidade e construir soluções que nos capacitem e permitam transformar o medo em Confiança, isolação em Conexão e aversão ao risco em Coragem."

# don't create a separate page
_build:
  render: "never"
---
