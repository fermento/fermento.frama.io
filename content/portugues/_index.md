---
# Banner
banner:
  title: "Mudar o mundo é um ato deliberado"
  content: "As pessoas que querem mudar o mundo enfrentam dificuldades políticas, culturais e técnicas. Por outro lado, têm uma história de séculos de experiência atrás delas. Construir em cima desta experiência, aprendendo com os sucessos e erros, estaríamos mais parte de ganhar."
  image: "/images/banner.png"
  image_darkmode: "/images/banner-darkmode.png"
  button:
    enable: true
    label: "Pedidos de Formações"
    link: "pedidos-formacoes"
---
